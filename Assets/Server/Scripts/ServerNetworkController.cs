﻿namespace TaskMaster.Server
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.UI;

    public class ServerNetworkController : MonoBehaviour
    {
        private string currentLog = "";
        private TMMediaItem NextMedia = null;
        private NetworkServerSimple server;
        private TMConfiguration configuration = null;

        private List<TMClientInfo> NetworkClients;

        // Start is called before the first frame update
        void Start()
        {
            // Turn off stack trace for debug logs
            Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);

            DontDestroyOnLoad(this);

            // Load configuration (TODO: configurable)
            configuration = TMConfiguration.Load(@"D:\Projects\dev\Unity\Taskmaster\GameData\taskmaster.xml");
            Debug.Log(string.Format("Loaded {0} players, {1} media items and {2} tasks", configuration.Players.Count, configuration.Media.Count, configuration.Tasks.Count));

            // Configure network
            NetworkClients = new List<TMClientInfo>();
            NetworkServer.Reset();
            server = new NetworkServerSimple();

            // Wire up Client connect/disconnect/identify events
            server.RegisterHandler(MsgType.Connect, OnConnect);
            server.RegisterHandler(MsgType.Disconnect, OnDisconnect);
            server.RegisterHandler(TaskMasterMsg.IdentifyResponse, OnIdentifyResponse);

            // Request to show scene
            server.RegisterHandler(TaskMasterMsg.ShowScene, OnShowScene);

            // Request to set selected media
            server.RegisterHandler(TaskMasterMsg.SetSelectedMedia, OnSetSelectedMedia);

            // Request to update score
            server.RegisterHandler(TaskMasterMsg.UpdateScore, OnUpdateScore);

            // Request to update task status
            server.RegisterHandler(TaskMasterMsg.TaskStatusUpdate, OnTaskStatusUpdate);

            // Acknowledgement of data received
            server.RegisterHandler(TaskMasterMsg.SendAck, OnAckReceived);

            // Start listening
            server.Listen(6550);
        }

        void Update()
        {
            if (server != null)
            {
                server.Update();
            }
        }

        /// <summary>
        /// Return the configuration
        /// </summary>
        /// <returns></returns>
        public TMConfiguration GetConfiguration()
        {
            return configuration;
        }

        void OnConnect(NetworkMessage netMsg)
        {
            // Add to our list of clients
            Debug.Log(string.Format("RX:OnConnect [CID:{0}]", netMsg.conn.connectionId));
            NetworkClients.Add(new TMClientInfo(netMsg.conn));

            // Send identification request
            var msg = new IdentifyRequestMessage();
            netMsg.conn.Send(TaskMasterMsg.IdentifyRequest, msg);
        }

        void OnDisconnect(NetworkMessage netMsg)
        {
            Debug.Log(string.Format("RX:OnDisconnect [CID:{0}]", netMsg.conn.connectionId));
            var clientinfo = GetClientByConnectionId(netMsg.conn.connectionId);
            NetworkClients.Remove(clientinfo);
        }

        /// <summary>
        /// Identification message
        /// </summary>
        /// <param name="netMsg"></param>
        void OnIdentifyResponse(NetworkMessage netMsg)
        {
            var client = NetworkClients.FirstOrDefault(t => t.Connection.connectionId == netMsg.conn.connectionId);

            var msg = netMsg.ReadMessage<IdentifyResponseMessage>();
            client.ClientType = msg.ClientType;
            client.ClientStatus = TMClientStatus.Intialising;
            Debug.Log(string.Format("RX:OnIdentifyResponse [CID: {0}; Type: {1}; Status:{2}]", netMsg.conn.connectionId, client.ClientType, client.ClientStatus));

            // Send configuration to the client
            SendConfiguration(netMsg.conn);
        }

        /// <summary>
        /// Score update received from remote controller
        /// </summary>
        /// <param name="netMsg"></param>
        void OnUpdateScore(NetworkMessage netMsg)
        {
            // Read the score, and update the player
            var msg = netMsg.ReadMessage<UpdateScoreMessage>();
            var player = configuration.Players.FirstOrDefault(p => p.Name == msg.PlayerName);
            player.Score = msg.Score;
            Debug.Log(string.Format("RX:OnUpdateScore [CID: {0}; Player:{1}; Score:{2}]", netMsg.conn.connectionId, player.Name, player.Score));

            // Send an update to the client to confirm
            var response = new UpdateScoreMessage()
            {
                PlayerName = player.Name,
                Score = player.Score
            };

            // Broadcast updates
            foreach (var client in NetworkClients)
            {
                // Send the request
                Debug.Log(string.Format("TX:UpdateScore [CID: {0}; Player:{1}; Score:{2}]", client.Connection.connectionId, player.Name, player.Score));
                client.Connection.Send(TaskMasterMsg.UpdateScore, msg);
            }

            // Save configuration
            configuration.Save();
        }

        /// <summary>
        /// Send current configuration to client
        /// </summary>
        /// <param name="netMsg"></param>
        void SendConfiguration(NetworkConnection connection)
        {
            // Locate client
            var clientInfo = GetClientByConnectionId(connection.connectionId);

            // If client is unknown, send request identify message
            if (clientInfo.ClientType == TMClientType.Unknown)
            {
                var identifyMsg = new IdentifyRequestMessage();
                connection.Send(TaskMasterMsg.IdentifyRequest, identifyMsg);
                return;
            }

            // Enqueue avatars in 60k chunks
            foreach (var player in configuration.Players.Where(p => p.Image != null))
            {
                var chunks = player.Image.Split(60000);
                var totalChunks = chunks.Count();
                var index = 1;
                foreach (var chunk in chunks)
                {
                    DataChunkMessage aMsg = new DataChunkMessage()
                    {
                        ChunkId = index,
                        ChunkType = TMDataChunkType.Avatar,
                        TotalChunks = totalChunks,
                        Name = player.Name,
                        Data = chunk
                    };

                    clientInfo.DataQueue.Enqueue(aMsg);
                    index++;
                }
            }

            // Enqueue media thumbnails in 60k chunks
            foreach (var media in configuration.Media.Where(p => p.Thumbnail != null))
            {
                var chunks = media.Thumbnail.Split(60000);
                var totalChunks = chunks.Count();
                var index = 1;
                foreach (var chunk in chunks)
                {
                    DataChunkMessage aMsg = new DataChunkMessage()
                    {
                        ChunkId = index,
                        ChunkType = TMDataChunkType.MediaThumbnail,
                        TotalChunks = totalChunks,
                        Name = media.Id.ToString(),
                        Data = chunk
                    };

                    clientInfo.DataQueue.Enqueue(aMsg);
                    index++;
                }
            }

            // Send the configuration
            Debug.Log(string.Format("TX:SendConfiguration [CID:{0}]", connection.connectionId));
            var msg = new ConfigDataMessage()
            {
                Config = SerializationHelper.SerializeXmlAsString(configuration)
            };

            connection.Send(TaskMasterMsg.ConfigData, msg);
        }

        /// <summary>
        /// On acknowledge of data receive, send the next chunk
        /// </summary>
        /// <param name="netMsg">Network mesage</param>
        private void OnAckReceived(NetworkMessage netMsg)
        {
            // Locate client
            var clientInfo = GetClientByConnectionId(netMsg.conn.connectionId);
            if (clientInfo.DataQueue.Count == 0)
            {
                // Send configuration complete and set client status to Ready
                clientInfo.Connection.Send(TaskMasterMsg.ConfigComplete, new ConfigCompleteMessage());
                clientInfo.ClientStatus = TMClientStatus.Ready;
            }
            else
            {
                var aMsg = clientInfo.DataQueue.Dequeue();
                Debug.Log(string.Format("Sending DataChunk {0} {1}/{2}", aMsg.Name, aMsg.ChunkId, aMsg.TotalChunks));
                clientInfo.Connection.Send(TaskMasterMsg.DataChunk, aMsg);
            }
        }

        void OnShowScene(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<ShowSceneMessage>();
            Debug.Log(string.Format("RX:OnShowScene [CID:{0}; Name:{1}]", netMsg.conn.connectionId, msg.SceneName));

            // Broadcast to all scoreboards
            foreach (var client in NetworkClients.Where(c => c.ClientType == TMClientType.Scoreboard))
            {
                // Send the request
                Debug.Log(string.Format("TX:ShowScene [CID: {0}; Name:{1}]", client.Connection.connectionId, msg.SceneName));
                client.Connection.Send(TaskMasterMsg.ShowScene, msg);
            }
        }

        void OnTaskStatusUpdate(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<TaskStatusUpdateMessage>();
            Debug.Log(string.Format("RX:OnTaskStatusUpdate [CID:{0}; TaskId:{1}; Active:{2}; Complete:{3}]", netMsg.conn.connectionId, msg.TaskId, msg.Active, msg.Complete));
            var task = configuration.Tasks.FirstOrDefault(t => t.Id == msg.TaskId);
            task.Active = msg.Active;
            task.Complete = msg.Complete;
            configuration.Save();

            // Broadcast updates to all clients, this also serves as an acknowledge to the source client
            foreach (var client in NetworkClients)
            {
                // Send the request
                Debug.Log(string.Format("TX:OnTaskStatusUpdate [CID:{0}; TaskId:{1}; Active:{2}; Complete:{3}]", client.Connection.connectionId, msg.TaskId, msg.Active, msg.Complete));
                client.Connection.Send(TaskMasterMsg.TaskStatusUpdate, msg);
            }
        }

        void OnSetSelectedMedia(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<SetSelectedMediaMessage>();
            var media = configuration.Media.FirstOrDefault(v => v.Id == msg.MediaId);

            // Save selection
            Debug.Log(string.Format("RX:OnSetSelectedMedia [CID:{0}; MediaId:{1}; Name:{2}]", netMsg.conn.connectionId, media.Id, media.Name));
            foreach (var v in configuration.Media)
            {
                v.Selected = v.Id == msg.MediaId;
            }

            configuration.Save();

            // Broadcast to all clients
            foreach (var client in NetworkClients)
            {
                Debug.Log(string.Format("TX:SetSelectedMedia [CID:{0}; MediaId:{1}; Name:{2}]", client.Connection.connectionId, media.Id, media.Name));
                client.Connection.Send(TaskMasterMsg.SetSelectedMedia, msg);
            }
        }

        /// <summary>
        /// Locate client info by ConnectionId
        /// </summary>
        /// <param name="connectionId">Connection Id</param>
        /// <returns>Client info if available</returns>
        TMClientInfo GetClientByConnectionId(int connectionId)
        {
            return NetworkClients.FirstOrDefault(t => t.Connection.connectionId == connectionId);
        }
    }
}