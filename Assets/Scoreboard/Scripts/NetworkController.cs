﻿namespace TaskMaster.Scoreboard
{
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.SceneManagement;

    public class NetworkController : TMNetworkClient
    {
        private string nextScene;

        void OnAwake()
        {
        }

        void OnEnable()
        {
            NetStart();
            Connect();
        }

        void OnLevelWasLoaded()
        {
            var animator = GetComponent<Animator>();
            animator.SetTrigger("FadeIn");
        }

        public override void OnShowScene(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<ShowSceneMessage>();
            SetMessage("Show Scene: " + msg.SceneName);
            nextScene = msg.SceneName;
            var animator = GetComponent<Animator>();
            animator.SetTrigger("FadeOut");
        }

        public void ChangeScene()
        {
            SceneManager.LoadScene(nextScene);
        }
    }
}