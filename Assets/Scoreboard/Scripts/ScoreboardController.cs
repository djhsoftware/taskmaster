﻿namespace TaskMaster.Scoreboard
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;

    public class ScoreboardController : MonoBehaviour
    {
        public GameObject FramePrefab;
        public GameObject YellowBubblePrefab;
        public GameObject RedBubblePrefab;
        public bool Visible;
        public AudioClip InSound;
        public AudioClip OutSound;

        private bool isVisible;
        private AudioSource audioSrc;
        private TMConfiguration configuration;
        private List<GameObject> frames;
        private List<GameObject> bubbles;

        void Awake()
        {
            var bgImage = GameObject.Find("Background");
            SpriteRenderer spriteRenderer = bgImage.GetComponent<SpriteRenderer>();
            float cameraHeight = Camera.main.orthographicSize * 2;
            Vector2 cameraSize = new Vector2(Camera.main.aspect * cameraHeight, cameraHeight);
            Vector2 spriteSize = spriteRenderer.sprite.bounds.size;

            Vector2 scale = transform.localScale;
            if (cameraSize.x >= cameraSize.y)
            { // Landscape (or equal)
                scale *= cameraSize.x / spriteSize.x;
            }
            else
            { // Portrait
                scale *= cameraSize.y / spriteSize.y;
            }

            bgImage.transform.localScale = scale;
        }

        // Start is called before the first frame update
        void Start()
        {
            // Save the audio source for play sounds
            audioSrc = gameObject.GetComponent<AudioSource>();

            // Locate the network controller
            var networkObj = GameObject.Find("NetworkController");
            if (networkObj != null)
            {
                // Get sceneController
                var networkController = networkObj.GetComponent<NetworkController>();

                // Load the configuration from scene controller
                configuration = networkController.GetConfiguration();
            }

            // Calculate screen width for distribution
            var worldScreenHeight = Camera.main.orthographicSize * 2.0f;
            var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            // Magic numbers :/
            var yStartOffset = -30f;
            var xOffset = -10f;
            var margin = worldScreenWidth / 5f;            // Was 20
            var yellowBubbleOffset = margin / 4;               // Was 2
            var scaleFactor = worldScreenWidth / 80f;
            var scale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
            var usableWidth = worldScreenWidth - (margin * 2.0f) - yellowBubbleOffset;
            var frameSegment = usableWidth / configuration.Players.Count;
            var baseFrameOffset = 0.5f;

            var bubbleYPosition = baseFrameOffset - scaleFactor * 9.0f;
            var bubbleScaleFactor = worldScreenWidth / 70f;
            var bubbleScale = new Vector3(bubbleScaleFactor, bubbleScaleFactor, bubbleScaleFactor);

            // Configure the scoreboard
            frames = new List<GameObject>();
            bubbles = new List<GameObject>();
            int index = 0;
            float offset = -(usableWidth / 2.0f) + margin - yellowBubbleOffset;
            offset += xOffset;
            foreach (var player in configuration.Players.OrderBy(p => p.Score))
            {
                GameObject bubblePrefab;
                Vector3 framePosition;
                Vector3 bubblePosition;
                Vector3 frameScale;
                float frameOffset;

                if (index < configuration.Players.Count - 1)
                {
                    bubblePrefab = RedBubblePrefab;
                    framePosition = new Vector3(offset, yStartOffset, -0.5f);
                    bubblePosition = new Vector3(offset, bubbleYPosition, -0.5f);
                    frameScale = scale;
                    frameOffset = baseFrameOffset;
                }
                else
                {
                    bubblePrefab = YellowBubblePrefab;
                    framePosition = new Vector3(offset + yellowBubbleOffset, yStartOffset, -0.5f);
                    bubblePosition = new Vector3(offset + yellowBubbleOffset, bubbleYPosition, -0.5f);
                    frameScale = scale * 1.3f;
                    frameOffset = baseFrameOffset + (scaleFactor * baseFrameOffset);
                }

                var frame = (GameObject)Instantiate(FramePrefab, framePosition, Quaternion.identity);
                frame.transform.localScale = frameScale;
                var frameController = frame.GetComponent<FrameController>();
                frameController.Offset = frameOffset;
                frameController.Visible = false;
                frameController.SetPlayer(player);
                frames.Add(frame);

                var bubble = (GameObject)Instantiate(bubblePrefab, bubblePosition, Quaternion.identity);
                bubble.GetComponent<BubbleController>().Score = player.Score;
                bubble.GetComponent<BubbleController>().Visible = false;
                bubble.transform.localScale = bubbleScale;
                bubbles.Add(bubble);

                offset += frameSegment;
                index++;
            }

            // Start the scoreboard
            StartCoroutine(SetVisible());
        }

        // Update is called once per frame
        void Update()
        {
            if (Visible != isVisible)
            {
                if (!Visible)
                {
                    audioSrc.clip = OutSound;
                    audioSrc.Play();
                }

                foreach (var frame in frames)
                {
                    var frameController = frame.GetComponent<FrameController>();
                    frameController.Visible = Visible;
                }

                StartCoroutine(ShowBubbles());
            }


            isVisible = Visible;
        }

        IEnumerator SetVisible()
        {
            yield return new WaitForSeconds(1);
            Visible = true;
        }

        IEnumerator ShowBubbles()
        {
            if (Visible)
            {
                yield return new WaitForSeconds(1);
                audioSrc.clip = InSound;
                audioSrc.Play();
            }

            foreach (var bubble in bubbles)
            {
                var bubbleController = bubble.GetComponent<BubbleController>();
                bubbleController.Visible = Visible;
            }
        }
    }
}