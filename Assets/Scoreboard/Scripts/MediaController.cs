﻿namespace TaskMaster.Scoreboard
{
    using System.Collections;
    using System.IO;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.Video;

    public class MediaController : MonoBehaviour
    {
        public VideoPlayer videoPlayer;
        public SpriteRenderer spriteRenderer;

        private NetworkController networkController;

        void Start()
        {
        }

        // Start is called before the first frame update
        void OnEnable()
        {
            var networkObj = GameObject.Find("NetworkController");
            networkController = networkObj.GetComponent<NetworkController>();

            videoPlayer.loopPointReached += EndReached;

            // Locate the network controller
            if (networkController == null)
            {
                Debug.Log("networkObj=null");
                StartCoroutine(ReturnToIdle());
                return;
            }

            if (networkController.SelectedMedia == null)
            {
                Debug.Log("selectedMedia=null");
                StartCoroutine(ReturnToIdle());
                return;
            }
            // Get sceneController
            if (!File.Exists(networkController.SelectedMedia.MediaPath))
            {
                Debug.Log("MediaNotFound");
                StartCoroutine(ReturnToIdle());
                return;
            }

            switch (networkController.SelectedMedia.Type)
            {
                case Common.MediaType.Image:
                    ShowImage(networkController.SelectedMedia.MediaPath);
                    break;
                case Common.MediaType.Video:
                    ShowVideo(networkController.SelectedMedia.MediaPath);
                    break;
                default:
                    Debug.Log("InvalidMediaType");
                    StartCoroutine(ReturnToIdle());
                    break;
            }
        }

        void ShowVideo(string mediaPath)
        {
            var videoPath = "file://" + mediaPath;
            spriteRenderer.gameObject.SetActive(false);
            videoPlayer.gameObject.SetActive(true);
            Debug.Log("Load video: " + videoPath);
            videoPlayer.url = videoPath;
            videoPlayer.Play();
        }

        void ShowImage(string mediaPath)
        {
            var fileData = File.ReadAllBytes(mediaPath);
            spriteRenderer.sprite.texture.LoadImage(fileData);
            spriteRenderer.gameObject.SetActive(true);
            videoPlayer.gameObject.SetActive(false);
            Debug.Log("Load image: " + mediaPath);
        }

        void EndReached(VideoPlayer vp)
        {
            StartCoroutine(ReturnToIdle());
        }

        IEnumerator ReturnToIdle()
        {
            yield return new WaitForSeconds(5);
            SceneManager.LoadScene("Idle");
        }
    }
}