﻿namespace TaskMaster.Scoreboard
{
    using UnityEngine;

    public class BubbleController : MonoBehaviour
    {
        public GameObject bubble;
        public GameObject scoreText;
        public int Score;
        public bool Visible = false;

        private float Fade = 0f;
        private SpriteRenderer bubbleRenderer;
        private TextMesh scoreRenderer;
        private Color bubbleColour;
        private Color scoreColour;

        // Start is called before the first frame update
        void Start()
        {
            bubbleRenderer = bubble.GetComponent<SpriteRenderer>();
            scoreRenderer = scoreText.GetComponent<TextMesh>();
            bubbleColour = bubbleRenderer.color;
            scoreColour = scoreRenderer.color;
            bubbleRenderer.color = new Color(bubbleColour.a, bubbleColour.g, bubbleColour.b, 0);
            scoreRenderer.color = new Color(scoreColour.a, scoreColour.g, scoreColour.b, 0);
        }

        // Update is called once per frame
        void Update()
        {
            if (Visible && Fade < 1.0f)
            {
                scoreRenderer.text = Score.ToString();
                Fade += 0.02f;
                scoreRenderer.color = new Color(scoreColour.a, scoreColour.g, scoreColour.b, Fade);
                bubbleRenderer.color = new Color(bubbleColour.a, bubbleColour.g, bubbleColour.b, Fade);
            }

            if (!Visible && Fade > 0)
            {
                Fade += -0.04f;
                scoreRenderer.color = new Color(scoreColour.a, scoreColour.g, scoreColour.b, Fade);
                bubbleRenderer.color = new Color(bubbleColour.a, bubbleColour.g, bubbleColour.b, Fade);
            }
        }
    }
}