﻿namespace TaskMaster.Scoreboard
{
    using System;
    using System.IO;
    using TaskMaster.Common;
    using UnityEngine;

    public class FrameController : MonoBehaviour
    {
        public bool Visible;
        public float Offset;
        public GameObject Portrait;

        private float speed = 30.0f;
        private Vector2 OffTarget;
        private Vector2 OnTarget;
        private Animator animator;
        private TMPlayer tmPlayer;
        private byte[] avatarBytes;

        void Start()
        {
            animator = gameObject.GetComponent<Animator>();
            OffTarget.x = gameObject.transform.position.x;
            OffTarget.y = -30;

            OnTarget.x = gameObject.transform.position.x;
            OnTarget.y = Offset;
            animator.speed += UnityEngine.Random.Range(-0.2f, 0.2f);
        }

        void Update()
        {
            // move sprite towards the target location
            var target = Visible ? OnTarget : OffTarget;
            float step = speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, target, step);
        }

        internal void SetPlayer(TMPlayer player)
        {
            tmPlayer = player;

            // Load Avatar if specified
            if (player.Image != null)
            {
                Texture2D texture = new Texture2D(400, 600, TextureFormat.RGB24, false);
                texture.filterMode = FilterMode.Trilinear;
                texture.LoadImage(player.Image);
                Sprite sprite = Sprite.Create(texture, new Rect(0, 0, 400, 600), new Vector2(0.5f, 0.5f), 100.0f);
                Portrait.GetComponent<SpriteRenderer>().sprite = sprite;
            }
        }
    }
}