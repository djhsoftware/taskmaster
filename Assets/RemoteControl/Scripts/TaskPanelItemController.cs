﻿namespace TaskMaster.Remote
{
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.UI;

    public class TaskPanelItemController : MonoBehaviour
    {
        public GameObject TickMark;
        public Text TitleText;
        public Image Button;

        private string taskName;
        private int taskId;
        private bool DisplayIsActive;
        private bool DisplayIsComplete;
        private TMTask connectedTask;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (connectedTask != null && (DisplayIsComplete != connectedTask.Complete || DisplayIsActive != connectedTask.Active))
            {
                UpdateTask();
            }
        }

        public void OnClick()
        {
            var menuObj = GameObject.Find("MenuController");
            if (menuObj != null)
            {
                // Get networkController
                var menuController = menuObj.GetComponent<MenuController>();
                menuController.ShowTaskDetail(taskId);
            }
        }

        public void ConfigureWith(TMTask task)
        {
            connectedTask = task;
            taskName = task.Name;
            taskId = task.Id;
            TitleText.text = string.Format("{0}. {1}", taskId, taskName);
            UpdateTask();
        }

        private void UpdateTask()
        {
            DisplayIsActive = connectedTask.Active;
            DisplayIsComplete = connectedTask.Complete;
            Button.color = connectedTask.Active ? Color.green : Color.white;
            TickMark.SetActive(connectedTask.Complete);
        }
    }
}