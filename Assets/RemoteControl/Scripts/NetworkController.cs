﻿namespace TaskMaster.Remote
{
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.UI;
    using Debug = UnityEngine.Debug;

    public class NetworkController : TMNetworkClient
    {
        public Text ConfigMessage;
        public Text VersionMessage;
        public bool MediaFilterCurrentTask = false;
        public MediaType MediaFilter = MediaType.All;
        public GameObject SettingsPanel;

        /// <summary>
        /// Use this for testing only!! Configuration should be downloaded from the server!
        /// </summary>
        public void Load()
        {
            Configuration = TMConfiguration.Load(@"D:\Projects\dev\Unity\Taskmaster\GameData\taskmaster.xml");
        }

        void Start()
        {
            NetStart();

            VersionMessage.text = GetBuildInfo();
            if (ProgressBar != null)
            {
                progressBarImage = ProgressBar.GetComponentsInChildren<Image>().Where(i => i.gameObject.name == "Foreground").FirstOrDefault();
            }

            UpdateConnectionInfo(Settings.Server, Settings.Port);
        }

        private void SetLoadProgress(float delta)
        {
            progressBarImage.fillAmount += delta;
        }

        private void UpdateConnectionInfo(string server, int port)
        {
            Settings.Server = server;
            Settings.Port= port;
            Settings.Save();
            GameObject.Find("ServerNameDisplay").GetComponent<Text>().text = string.Format("{0}:{1}", Settings.Server, Settings.Port);
        }

        public void QuitApplication()
        {
            Application.Quit();
        }

        public void OpenSettings()
        {
            SettingsPanel.transform.Find("ServerEntryField").GetComponent<InputField>().text = Settings.Server;
            SettingsPanel.transform.Find("PortEntryField").GetComponent<InputField>().text = Settings.Port.ToString();
            SettingsPanel.SetActive(true);
        }

        public void SaveSettings()
        {
            var newServer = SettingsPanel.transform.Find("ServerEntryField").GetComponent<InputField>().text;
            var newPort = int.Parse(SettingsPanel.transform.Find("PortEntryField").GetComponent<InputField>().text);
            UpdateConnectionInfo(newServer, newPort);
            SettingsPanel.SetActive(false);
        }

        public void CloseSettings()
        {
            SettingsPanel.SetActive(false);
        }

        #region Update events
        /// <summary>
        /// Send a score update back to host
        /// </summary>
        /// <param name="playerName"></param>
        /// <param name="score"></param>
        public void UpdateScore(string playerName, int score)
        {
            var msg = new UpdateScoreMessage()
            {
                PlayerName = playerName,
                Score = score
            };

            Debug.Log(string.Format("TX:UpdateScore [Player:{0} Score:{1}]", msg.PlayerName, msg.Score));
            client.Send(TaskMasterMsg.UpdateScore, msg);
        }

        public override void SetMessage(string message)
        {
            if (ConfigMessage != null)
            {
                ConfigMessage.text = message;
            }

            Debug.Log(message);
        }

        public void ShowScene(string scene)
        {
            var sceneMsg = new ShowSceneMessage()
            {
                SceneName = scene
            };

            Debug.Log(string.Format("TX:ShowScene [Name:{0}]", scene));
            client.Send(TaskMasterMsg.ShowScene, sceneMsg);
        }

        public void PlayMedia(TMMediaItem media)
        {
            SetSelectedMedia(media);
            ShowScene("MediaPlayer");
        }

        /// <summary>
        /// Set selected media event
        /// </summary>
        /// <param name="media">Media to select</param>
        public void SetSelectedMedia(TMMediaItem media)
        {
            if (client != null)
            {
                var sceneMsg = new SetSelectedMediaMessage()
                {
                    MediaId = media.Id
                };

                Debug.Log(string.Format("TX:SetSelectedMedia [MediaId:{0}]", media.Id));
                client.Send(TaskMasterMsg.SetSelectedMedia, sceneMsg);
            }
        }

        public void SetTaskStatus(TMTask task)
        {
            // Reset current active task
            if (task.Active)
            {
                var currentActiveTask = Configuration.Tasks.FirstOrDefault(t => t.Active && t.Id != task.Id);
                if (currentActiveTask != null)
                {
                    currentActiveTask.Active = false;
                    var inactiveMsg = new TaskStatusUpdateMessage()
                    {
                        TaskId = currentActiveTask.Id,
                        Complete = currentActiveTask.Complete,
                        Active = currentActiveTask.Active
                    };

                    Debug.Log(string.Format("TX:SetTaskStatus [Task:{0}; Active:{1}; Complete:{2}]", currentActiveTask.Id, currentActiveTask.Active, currentActiveTask.Complete));
                    client.Send(TaskMasterMsg.TaskStatusUpdate, inactiveMsg);
                }
            }

            Debug.Log(string.Format("TX:SetTaskStatus [Task:{0}; Active:{1}; Complete:{2}]", task.Id, task.Active, task.Complete));
            var msg = new TaskStatusUpdateMessage()
            {
                TaskId = task.Id,
                Complete = task.Complete,
                Active = task.Active
            };

            client.Send(TaskMasterMsg.TaskStatusUpdate, msg);
        }
        #endregion
    }
}