﻿namespace TaskMaster.Remote
{
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;

    public class MenuController : MonoBehaviour
    {
        public GameObject MenuScores;
        public GameObject MenuScenes;
        public GameObject MenuMedia;
        public GameObject MenuTasks;
        public GameObject MenuTaskDetail;

        public Button MenuScoreButton;
        public Button MenuScenesButton;
        public Button MenuMediaButton;
        public Button MenuTasksButton;

        public GameObject ExitConfirmYesButton;
        public GameObject ExitConfirmNoButton;
        
        private NetworkController networkController;

        private enum MenuItem
        {
            MenuItemScores,
            MenuItemScenes,
            MenuItemMedia,
            MenuItemTasks,
            MenuItemTaskDetail
        }

        // Start is called before the first frame update
        void Start()
        {
            var networkObj = GameObject.Find("NetworkController");
            if (networkObj != null)
            {
                // Get networkController
                networkController = networkObj.GetComponent<NetworkController>();
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void Disconnect()
        {
            networkController.Disconnect();
        }

        private void ShowMenu(MenuItem menu)
        {
            MenuScores.SetActive(menu == MenuItem.MenuItemScores);
            MenuScoreButton.GetComponent<Image>().color = menu == MenuItem.MenuItemScores ? Color.green : Color.white;

            MenuScenes.SetActive(menu == MenuItem.MenuItemScenes);
            MenuScenesButton.GetComponent<Image>().color = menu == MenuItem.MenuItemScenes ? Color.green : Color.white;

            MenuMedia.SetActive(menu == MenuItem.MenuItemMedia);
            MenuMediaButton.GetComponent<Image>().color = menu == MenuItem.MenuItemMedia ? Color.green : Color.white;

            MenuTasks.SetActive(menu == MenuItem.MenuItemTasks);
            MenuTasksButton.GetComponent<Image>().color = menu == MenuItem.MenuItemTaskDetail || menu == MenuItem.MenuItemTasks ? Color.green : Color.white;

            MenuTaskDetail.SetActive(menu == MenuItem.MenuItemTaskDetail);
        }

        public void ShowMenuScores()
        {
            ShowMenu(MenuItem.MenuItemScores);
        }

        public void ShowMenuScenes()
        {
            ShowMenu(MenuItem.MenuItemScenes);
        }

        public void ShowMenuMedia()
        {
            ShowMenu(MenuItem.MenuItemMedia);
        }

        public void ShowMenuTasks()
        {
            ShowMenu(MenuItem.MenuItemTasks);
        }

        public void ShowTaskDetail(int taskId)
        {
            Debug.Log(taskId);
            ShowMenu(MenuItem.MenuItemTaskDetail);
            var task = networkController.GetConfiguration().Tasks.FirstOrDefault(t => t.Id == taskId);
            MenuTaskDetail.GetComponent<TaskViewController>().SetTask(task);
        }

        public void ShowScoreboard()
        {
            networkController.ShowScene("Scoreboard");
        }

        public void ShowIdleScreen()
        {
            networkController.ShowScene("Idle");
        }

        public void ShowBlackScreen()
        {
            networkController.ShowScene("Online");
        }

        public void ShowSelectedMedia()
        {
            networkController.ShowScene("MediaClip");
        }

        public void ExitConfirm()
        {
            ExitConfirmYesButton.SetActive(true);
            ExitConfirmNoButton.SetActive(true);
        }

        public void ExitConfirmYes()
        {
            Application.Quit();
        }

        public void ExitConfirmNo()
        {
            ExitConfirmYesButton.SetActive(false);
            ExitConfirmNoButton.SetActive(false);
        }
    }
}