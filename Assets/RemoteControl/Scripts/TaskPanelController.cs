﻿namespace TaskMaster.Remote
{
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;

    public class TaskPanelController : NetworkUIController
    {
        public GameObject TaskPanelContent;
        public GameObject TaskPanelPrefab;

        void OnEnable()
        {
            InitNetwork();

            // Remove existing score panels
            foreach (Transform child in TaskPanelContent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            // Add score panels
            foreach (var task in configuration.Tasks.OrderBy(t => t.Id))
            {
                var taskPanel = (GameObject)Instantiate(TaskPanelPrefab, Vector3.zero, Quaternion.identity, TaskPanelContent.transform);
                var spc = taskPanel.GetComponent<TaskPanelItemController>();
                spc.ConfigureWith(task);
            }
        }
    }
}