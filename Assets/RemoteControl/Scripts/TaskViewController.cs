﻿namespace TaskMaster.Remote
{
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.UI;

    public class TaskViewController : MonoBehaviour
    {
        public Text Content;
        public Text Title;
        public GameObject ActiveButton;
        public GameObject CompleteButton;

        private NetworkController networkController;
        private TMTask selectedTask;
        private bool DisplayIsActive;
        private bool DisplayIsComplete;

        // Start is called before the first frame update
        void Start()
        {
            networkController = GameObject.Find("NetworkController").GetComponent<NetworkController>();
        }

        void Update()
        {
            if (selectedTask != null && (DisplayIsComplete != selectedTask.Complete || DisplayIsActive != selectedTask.Active))
            {
                UpdateButtonState();
            }
        }

        public void SetTask(TMTask task)
        {
            selectedTask = task;
            Title.text = task.Name;
            Content.text = task.Content.Trim();
            UpdateButtonState();
        }

        public void MarkAsActive()
        {
            selectedTask.Active = !selectedTask.Active;
            networkController.SetTaskStatus(selectedTask);
            UpdateButtonState();
        }

        public void MarkComplete()
        {
            selectedTask.Complete = !selectedTask.Complete;
            networkController.SetTaskStatus(selectedTask);
            UpdateButtonState();
        }

        public void ShowNextTask()
        {
            Debug.Log("Next");
            var task = networkController.GetConfiguration().Tasks.Where(t => t.Id > selectedTask.Id).OrderBy(t => t.Id).FirstOrDefault();
            if (task != null)
            {
                SetTask(task);
            }
        }

        public void ShowPreviousTask()
        {
            Debug.Log("Previous");
            var task = networkController.GetConfiguration().Tasks.Where(t => t.Id < selectedTask.Id).OrderByDescending(t => t.Id).FirstOrDefault();
            if (task != null)
            {
                SetTask(task);
            }
        }

        private void UpdateButtonState()
        {
            DisplayIsActive = selectedTask.Active;
            DisplayIsComplete = selectedTask.Complete;

            ActiveButton.GetComponent<Image>().color = selectedTask.Active ? Color.green : Color.white;
            ActiveButton.GetComponentInChildren<Text>().text = selectedTask.Active ? "Active" : "Inactive";

            CompleteButton.GetComponent<Image>().color = selectedTask.Complete ? Color.green : Color.white;
            CompleteButton.GetComponentInChildren<Text>().text = selectedTask.Complete ? "Complete" : "Incomplete";
        }
    }
}