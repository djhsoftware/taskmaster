﻿namespace TaskMaster.Remote
{
    using System;
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.UI;

    public class MediaPanelController : NetworkUIController
    {
        public GameObject MediaPanelContent;
        public GameObject MediaPanelPrefab;
        public GameObject ViewCurrentTaskButton;
        public GameObject ViewAllButton;

        void OnEnable()
        {
            InitNetwork();
            LoadMedia();
        }

        /// <summary>
        /// Show media relating to current task only
        /// </summary>
        public void ToggleShowCurrentTask()
        {
            networkController.MediaFilterCurrentTask = !networkController.MediaFilterCurrentTask;
            LoadMedia();
        }

        /// <summary>
        /// Show all media
        /// </summary>
        public void ToggleFilter()
        {
            int nextMediaType = (int)networkController.MediaFilter + 1;
            if (Enum.IsDefined(typeof(MediaType), nextMediaType))
            {
                networkController.MediaFilter = (MediaType)nextMediaType;
            }
            else
            {
                networkController.MediaFilter = MediaType.All;
            }

            LoadMedia();
        }

        void LoadMedia()
        {
            Debug.Log("Load media");

            var selectedMedia = configuration.Media.AsQueryable();

            // Setup active task filter
            ViewCurrentTaskButton.GetComponent<Image>().color = networkController.MediaFilterCurrentTask ? Color.green : Color.white;
            var activeTask = configuration.Tasks.FirstOrDefault(t => t.Active);
            if (activeTask != null && networkController.MediaFilterCurrentTask)
            {
                Debug.Log(string.Format("Filter to task {0}", activeTask.Id));
                selectedMedia = selectedMedia.Where(v => v.TaskNo == activeTask.Id);
            }

            // Setup media type filter
            if (networkController.MediaFilter == MediaType.Unknown)
            {
                networkController.MediaFilter = MediaType.All;
            }

            ViewAllButton.GetComponent<Image>().color = networkController.MediaFilter == MediaType.All ? Color.white : Color.green;
            ViewAllButton.GetComponentInChildren<Text>().text = networkController.MediaFilter.ToString();
            if (networkController.MediaFilter != MediaType.All)
            {
                Debug.Log(string.Format("Filter to MediaType {0}", networkController.MediaFilter));
                selectedMedia = selectedMedia.Where(v => v.Type == networkController.MediaFilter);
            }

            // Remove existing media panels
            foreach (Transform child in MediaPanelContent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            // Add score panels
            foreach (var media in selectedMedia.OrderBy(v => v.TaskNo))
            {
                var mediaPanel = (GameObject)Instantiate(MediaPanelPrefab, Vector3.zero, Quaternion.identity, MediaPanelContent.transform);
                var vpc = mediaPanel.GetComponent<MediaPanelItemController>();
                vpc.ConfigureWith(media);
            }
        }

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}