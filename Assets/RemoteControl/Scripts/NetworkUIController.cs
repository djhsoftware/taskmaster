﻿namespace TaskMaster.Remote
{
    using TaskMaster.Common;
    using UnityEngine;

    public abstract class NetworkUIController : MonoBehaviour
    {
        protected NetworkController networkController;
        protected TMConfiguration configuration;

        protected void InitNetwork()
        {
            // Locate the scene controller
            var networkObj = GameObject.Find("NetworkController");
            if (networkObj != null)
            {
                // Get networkController
                networkController = networkObj.GetComponent<NetworkController>();
                configuration = networkController.GetConfiguration();
            }
            else
            {
                // Emulated for testing only
                networkController = new NetworkController();
                networkController.Load();
                configuration = networkController.GetConfiguration();
                foreach (var player in configuration.Players)
                {
                    player.Score = Random.Range(23, 40);
                }
            }
        }
    }
}
