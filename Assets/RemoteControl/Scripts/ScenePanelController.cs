﻿namespace TaskMaster.Remote
{
    using System.Collections.Generic;
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.UI;

    public class ScenePanelController : NetworkUIController
    {
        public GameObject MediaSceneButton;
        private TMMediaItem selectedMedia = null;

        // Start is called before the first frame update
        void Start()
        {
            InitNetwork();
        }

        void OnEnable()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            if (gameObject.activeInHierarchy)
            {
                if (selectedMedia != networkController.SelectedMedia)
                {
                    selectedMedia = networkController.SelectedMedia;
                }
            }
        }
    }
}