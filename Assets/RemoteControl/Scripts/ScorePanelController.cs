﻿namespace TaskMaster.Remote
{
    using System.Collections.Generic;
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;

    public class ScorePanelController : NetworkUIController
    {
        public GameObject ScorePanelContent;
        public GameObject ScorePanelPrefab;
        private Dictionary<string, GameObject> scorePanels;

        void OnEnable()
        {
            InitNetwork();

            // Remove existing score panels
            foreach (Transform child in ScorePanelContent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            // Add score panels
            scorePanels = new Dictionary<string, GameObject>();
            foreach (var player in configuration.Players.OrderBy(p => p.Score).Reverse())
            {
                var scorePanel = (GameObject)Instantiate(ScorePanelPrefab, Vector3.zero, Quaternion.identity, ScorePanelContent.transform);
                var spc = scorePanel.GetComponent<ScorePanelItemController>();
                spc.ConfigureWith(player);
                scorePanels.Add(player.Name, scorePanel);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}