﻿namespace TaskMaster.Remote
{
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.UI;

    public class MediaPanelItemController : NetworkUIController
    {
        public Button Button;
        public Image mediaItemThumbnail;
        public Text mediaItemName;
        public Text taskNo;
        public Text playerNames;

        private TMMediaItem thisMediaItem;
        private bool displaySelected;

        // Start is called before the first frame update
        void Start()
        {
        }

        void OnEnable()
        {
            InitNetwork();
        }

        void Update()
        {
            if (displaySelected != thisMediaItem.Selected)
            {
                SetButtonState();
            }
        }

        internal void ConfigureWith(TMMediaItem media)
        {
            media.Controller = this;
            thisMediaItem = media;
            mediaItemName.text = media.Name;
            if (media.TaskNo > 0)
            {
                taskNo.text = string.Format("Task #{0}", media.TaskNo);
                playerNames.text = media.Players;
            }
            else
            {
                taskNo.text = string.Empty;
                playerNames.text = string.Empty;
            }

            // Load Thumbnail if specified
            if (media.Thumbnail != null)
            {
                Texture2D texture = new Texture2D(300, 200, TextureFormat.RGB24, false);
                texture.filterMode = FilterMode.Trilinear;
                texture.LoadImage(media.Thumbnail);
                Sprite sprite = Sprite.Create(texture, new Rect(0, 0, 300, 200), new Vector2(0.5f, 0.5f), 100.0f);
                mediaItemThumbnail.sprite = sprite;
            }

            // Set status
            SetButtonState();
        }

        public void SetButtonState()
        {
            Button.GetComponent<Image>().color = thisMediaItem.Selected ? Color.green : Color.white;
            displaySelected = thisMediaItem.Selected;
        }

        public void OnClick()
        {
            networkController.SetSelectedMedia(thisMediaItem);
        }

        public void OnEnqueue()
        {
        }

        public void OnPlay()
        {
            networkController.PlayMedia(thisMediaItem);
        }
    }
}