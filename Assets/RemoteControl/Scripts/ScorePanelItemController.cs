﻿namespace TaskMaster.Remote
{
    using System.Linq;
    using TaskMaster.Common;
    using UnityEngine;
    using UnityEngine.UI;

    public class ScorePanelItemController : NetworkUIController
    {
        public Text PlayerName;
        public Text PlayerScore;
        public Image Portrait;

        private int deltaScore;
        private int displayScore;
        private TMPlayer connectedPlayer;

        // Start is called before the first frame update
        void Start()
        {
        }

        void OnEnable()
        {
            InitNetwork();
        }

        // Update is called once per frame
        void Update()
        {
            // Update only if changed
            if (connectedPlayer != null && displayScore != connectedPlayer.Score)
            {
                deltaScore = 0;
                displayScore = connectedPlayer.Score;
                UpdateScoreDisplay();
            }
        }

        public void ConfigureWith(TMPlayer player)
        {
            Debug.Log(string.Format("Configure with {0}:{1}", player.Name, player.Score));
            connectedPlayer = player;
            PlayerName.text = player.Name;
            deltaScore = 0;
            displayScore = connectedPlayer.Score;
            UpdateScoreDisplay();

            // Load Avatar if specified
            if (player.Image != null)
            {
                Texture2D texture = new Texture2D(200, 300, TextureFormat.RGB24, false);
                texture.filterMode = FilterMode.Trilinear;
                texture.LoadImage(player.Image);
                Sprite sprite = Sprite.Create(texture, new Rect(0, 0, 200, 300), new Vector2(0.5f, 0.5f), 100.0f);
                Portrait.sprite = sprite;
            }
        }

        public void IncrementScore()
        {
            deltaScore++;
            UpdateScoreDisplay();
        }

        public void DecrementScore()
        {
            deltaScore--;
            UpdateScoreDisplay();
        }

        public void SaveScore()
        {
            if (connectedPlayer != null && deltaScore != 0)
            {
                networkController.UpdateScore(connectedPlayer.Name, connectedPlayer.Score + deltaScore);
            }
        }

        private void UpdateScoreDisplay()
        {
            PlayerScore.text = string.Format("{0}{1}", displayScore, deltaScore != 0 ? "(" + deltaScore.ToString() + ")" : string.Empty);
        }
    }
}