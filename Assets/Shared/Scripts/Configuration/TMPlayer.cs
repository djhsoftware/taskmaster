﻿namespace TaskMaster.Common
{
    using System;
    using System.IO;
    using System.Xml.Serialization;

    public class TMPlayer
    {
        private string avatarFilename;

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Avatar
        {
            get
            {
                return avatarFilename;
            }

            set
            {
                avatarFilename = value;
                if (File.Exists(avatarFilename))
                {
                    Image = File.ReadAllBytes(avatarFilename);
                }
                else
                {
                    Image = null;
                }
            }
        }

        [XmlAttribute]
        public int Score { get; set; }

        [XmlIgnore]
        public byte[] Image { get; set; }

        [XmlIgnore]
        public string ImageData
        {
            get
            {
                return Convert.ToBase64String(Image);
            }
            set
            {
                Image = Convert.FromBase64String(value);
            }
        }
    }
}