﻿
namespace TaskMaster.Common
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using TaskMaster.Remote;
    using UnityEngine;
    using Debug = UnityEngine.Debug;

    public enum MediaType
    {
        Unknown = -1,
        All = 0,
        Image = 1,
        Video = 2
    }

    public class TMMediaItem
    {
        private string path;
        private static readonly string[] VideoExtensions = { ".mp4", ".mov", ".mpg", ".mpeg" };
        private static readonly string[] ImageExtensions = { ".jpg", ".png", ".bmp", ".jpeg" };

        [XmlAttribute]
        public int Id { get; set; }

        [XmlAttribute]
        public int TaskNo { get; set; }

        [XmlAttribute]
        public string Players { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public bool Selected { get; set; }

        [XmlAttribute(AttributeName = "Path")]
        public string MediaPath
        {
            get
            {
                return path;
            }

            set
            {
                path = value;
                Thumbnail = LoadOrGenerateThumbnail(path);
            }
        }

        [XmlIgnore]
        public MediaType Type { get; set; }

        [XmlIgnore]
        public byte[] Thumbnail { get; set; }

        [XmlIgnore]
        public string ThumbnailData
        {
            get
            {
                return Convert.ToBase64String(Thumbnail);
            }
            set
            {
                Thumbnail = Convert.FromBase64String(value);
            }
        }

        [XmlIgnore]
        public MediaPanelItemController Controller { get; set; }

        /// <summary>
        /// Generate or load thumbnail for media
        /// </summary>
        /// <param name="path">Media path</param>
        /// <returns>thumbnail image data</returns>
        public byte[] LoadOrGenerateThumbnail(string path)
        {
            var thumbnailPath = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path) + ".thm");
            if (File.Exists(path))
            {
                var extension = Path.GetExtension(path);
                if (VideoExtensions.Contains(extension))
                {
                    Type = MediaType.Video;
                    GenerateVideoThumbnail(path, thumbnailPath);
                }
                else if (ImageExtensions.Contains(extension))
                {
                    Type = MediaType.Image;
                    GenerateImageThumbnail(path, thumbnailPath);
                }
                else
                {
                    Type = MediaType.Unknown;
                    Debug.Log(string.Format("Unknown media extension: {0}", extension));
                }
            }
            else
            {
                Debug.Log("*********************************************************************************");
                Debug.Log("WARNING FILE NOT FOUND!");
                Debug.Log(string.Format("    IMAGE : {0}", path));
                Debug.Log("*********************************************************************************");
                return null;
            }

            // If file exists, read it
            if (File.Exists(thumbnailPath))
            {
                // Check filesize, delete if zero bytes
                var fi = new FileInfo(thumbnailPath);
                if (fi.Length == 0)
                {
                    File.Delete(thumbnailPath);
                    return null;
                }
                else
                {
                    return File.ReadAllBytes(thumbnailPath);
                }
            }
            else
            {
                return null;
            }
        }

        private void ResizeImage(string src, string dest, int width, int height)
        {
            var imagemagick = @"D:\media\Software\imagemagick\convert.exe";
            var args = string.Format("\"{0}\" -resize {1}x{2} \"{3}\"", src, width, height, dest);
            RunCommand(imagemagick, args);
        }

        private void GenerateImageThumbnail(string path, string thumbnailPath)
        {
            if (File.Exists(thumbnailPath)) return;

            Debug.Log(string.Format("Generating thumbnail for: {0}", path));
            var fileData = File.ReadAllBytes(path);
            var tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            if (tex.width != 1920 || tex.height != 1080)
            {
                Debug.Log("*********************************************************************************");
                Debug.Log("WARNING TEXTURE SIZE IS WRONG!");
                Debug.Log(string.Format("    IMAGE : {0}", path));
                Debug.Log(string.Format("    SIZE  : {0}x{1}", tex.width, tex.height));
                Debug.Log("*********************************************************************************");
            }

            TextureScale.Bilinear(tex, 360, 200);
            byte[] bytes = tex.EncodeToJPG();
            File.WriteAllBytes(thumbnailPath, bytes);
        }

        private void GenerateVideoThumbnail(string path, string thumbnailPath)
        {
            if (File.Exists(thumbnailPath)) return;

            var ffmpeg = @"D:\Media\Software\ffmpeg\bin\ffmpeg.exe";
            var args = string.Format("-itsoffset -5  -i \"{0}\" -vcodec mjpeg -vframes 1 -an -f rawvideo -s 360x200 \"{1}\"", path, thumbnailPath);
            RunCommand(ffmpeg, args);
        }

        private void RunCommand(string command, string args)
        {
            Debug.Log(string.Format("CMD: {0} {1}", command, args));
            var pi = new ProcessStartInfo();
            pi.Arguments = args;
            pi.UseShellExecute = false;
            pi.CreateNoWindow = true;
            pi.FileName = command;
            pi.RedirectStandardOutput = true;
            pi.RedirectStandardError = true;
            pi.WindowStyle = ProcessWindowStyle.Hidden;
            var proc = Process.Start(pi);
            proc.WaitForExit();
            Debug.Log("stdout: " + proc.StandardOutput.ReadToEnd());
            Debug.Log("stderr: " + proc.StandardError.ReadToEnd());

        }
    }
}
