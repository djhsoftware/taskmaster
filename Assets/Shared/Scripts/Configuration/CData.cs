﻿namespace TaskMaster.Common
{
    using System;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// CDATAContent wrapper for XML Serialization
    /// </summary>
    public class CDATA
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CDATA" /> class.
        /// </summary>
        /// <param name="content">cdata content</param>
        public CDATA(string content)
        {
            this.Content = content;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CDATA" /> class.
        /// </summary>
        /// <param name="content">cdata content</param>
        public CDATA(string content, string type)
        {
            this.Content = content;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CDATA" /> class.
        /// </summary>
        public CDATA()
        {
        }


        /// <summary>
        /// Gets or sets string content
        /// </summary>
        [XmlIgnore]
        public string Content { get; set; }


        /// <summary>
        /// Gets or sets CDATA content
        /// </summary>
        [XmlText]
        public XmlNode[] CDataContent
        {
            get
            {
                var dummy = new XmlDocument();
                return Content == null ? null : new XmlNode[] { dummy.CreateCDataSection(Content) };
            }

            set
            {
                if (value == null)
                {
                    Content = null;
                    return;
                }

                if (value.Length != 1)
                {
                    throw new InvalidOperationException(string.Format("Invalid array length {0}", value.Length));
                }

                Content = value[0].Value;
            }
        }
    }

}
