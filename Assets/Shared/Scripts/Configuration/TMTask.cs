﻿using System.Xml.Serialization;

namespace TaskMaster.Common
{
    public class TMTask : CDATA
    {
        [XmlAttribute]
        public int Id { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public bool Complete { get; set; }

        [XmlAttribute]
        public bool Active { get; set; }
    }
}