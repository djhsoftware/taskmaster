﻿namespace TaskMaster.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;
    using UnityEngine;

    [Serializable]
    public class TMConfiguration
    {
        [XmlArrayItem("Player")]
        public List<TMPlayer> Players;

        [XmlArrayItem("Media")]
        public List<TMMediaItem> Media;

        [XmlArrayItem("Task")]
        public List<TMTask> Tasks;

        [XmlArrayItem("MediaId")]
        public List<int> Playlist;

        /// <summary>
        /// Configuration filename
        /// </summary>
        [XmlIgnore]
        public string Filename { get; private set; }

        public static TMConfiguration Load(string filename)
        {
            Debug.Log(string.Format("Load configuration: {0}", filename));
            var config = SerializationHelper.DeserializeXmlFromFile<TMConfiguration>(filename);
            int mediaId = 1;
            config.Media.ForEach(m => m.Id = mediaId++);
            config.Filename = filename;
            return config;
        }

        public void Save()
        {
            Debug.Log(string.Format("Save configuration: {0}", Filename));
            SerializationHelper.SerializeXmlToFile<TMConfiguration>(this, Filename);
        }
    }
}