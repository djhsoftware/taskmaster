﻿namespace TaskMaster.Common
{
    using UnityEngine;

    public class ConsoleToGUI : MonoBehaviour
    {
        //#if !UNITY_EDITOR
        static string myLog = "";
        private string output;
        static int bufferSize = 400;

        void OnEnable()
        {
            Application.logMessageReceived += Log;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= Log;
        }

        public void Log(string logString, string stackTrace, LogType type)
        {
            output = logString;
            myLog = output + "\n" + myLog;
            if (myLog.Length > bufferSize)
            {
                myLog = myLog.Substring(0, bufferSize);
            }
        }

        void OnGUI()
        {
            //if (!Application.isEditor) //Do not display in editor ( or you can use the UNITY_EDITOR macro to also disable the rest)
            {
                myLog = GUI.TextArea(new Rect(10, Screen.height - 100, Screen.width - 20, 90), myLog);
            }
        }
        //#endif
    }
}