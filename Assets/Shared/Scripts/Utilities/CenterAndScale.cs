﻿namespace TaskMaster.Common
{
    using UnityEngine;

    public class CenterAndScale : MonoBehaviour
    {
        public float Scale = 1.0f;
        public bool Proportional = true;

        // Start is called before the first frame update
        void Start()
        {
            var sr = GetComponent<SpriteRenderer>();
            if (sr == null) return;

            transform.localScale = new Vector3(1, 1, 1);

            var width = sr.sprite.bounds.size.x;
            var height = sr.sprite.bounds.size.y;

            var worldScreenHeight = Camera.main.orthographicSize * 2.0f;
            var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            if (Proportional)
            {
                var newScale = (worldScreenHeight / height) * Scale;
                transform.localScale = new Vector3(newScale, newScale, 1);
            }
            else
            {
                var newHeight = (worldScreenHeight / height) * Scale;
                var newWidth = (worldScreenWidth / width) * Scale;
                transform.localScale = new Vector3(newWidth, newHeight, 1) * 1.01f;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}