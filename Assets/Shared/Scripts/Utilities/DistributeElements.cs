﻿namespace TaskMaster.Common
{
    using UnityEngine;

    public class DistributeElements : MonoBehaviour
    {
        public int offsetLeft;
        public int offsetRight;

        public GameObject[] objects;

        // Start is called before the first frame update
        void Start()
        {
            var sr = GetComponent<SpriteRenderer>();
            if (sr == null) return;

            transform.localScale = new Vector3(1, 1, 1);

            var width = sr.sprite.bounds.size.x;
            var height = sr.sprite.bounds.size.y;

            var worldScreenHeight = Camera.main.orthographicSize * 2.0f;
            var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            var usableWidth = worldScreenWidth - offsetLeft - offsetRight;
            var segmentWidth = usableWidth / objects.Length;

            var xStart = -(worldScreenWidth / 2) + offsetLeft;
            float x = xStart;
            for (int index = 0; index < objects.Length; index++)
            {
                var newPos = new Vector3(x, objects[index].transform.position.y, objects[index].transform.position.z);
                objects[index].transform.position = newPos;
                x += segmentWidth;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}