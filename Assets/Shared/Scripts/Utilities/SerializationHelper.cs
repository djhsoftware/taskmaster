﻿namespace TaskMaster.Common
{
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using System.Xml.Serialization;

    /// <summary>
    /// XML Serialization helper
    /// </summary>
    public static class SerializationHelper
    {
        #region binary

        /// <summary>
        /// The serialize.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static Stream Serialize(Type type, object dto)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var bf = new BinaryFormatter();
            var stream = new MemoryStream();
            bf.Serialize(stream, dto);

            return stream;
        }

        /// <summary>
        /// The serialize.
        /// </summary>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static Stream Serialize<T>(T dto)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var bf = new BinaryFormatter();
            var stream = new MemoryStream();
            bf.Serialize(stream, dto);

            return stream;
        }

        /// <summary>
        /// The serialize.
        /// </summary>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static void Serialize<T>(T dto, Stream stream)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var bf = new BinaryFormatter();
            bf.Serialize(stream, dto);
        }

        /// <summary>
        /// The deserialize.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static T Deserialize<T>(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var bf = new BinaryFormatter();
            var dto = (T)bf.Deserialize(stream);
            return dto;
        }

        /// <summary>
        /// The deserialize.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static object Deserialize(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var bf = new BinaryFormatter();
            var dto = bf.Deserialize(stream);
            return dto;
        }

        /// <summary>
        /// Deserialize from file
        /// </summary>
        /// <typeparam name="T">The <see cref="object"/>.</typeparam>
        /// <param name="filename">Filename containing XML</param>
        /// <returns>new Object</returns>
        public static T DeserializeXmlFromFile<T>(string filename)
        {
            var data = File.ReadAllText(filename);
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
            {
                return DeserializeXml<T>(stream);
            }
        }

        #endregion

        #region xml

        /// <summary>
        /// The serialize xml as byte.
        /// </summary>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static byte[] SerializeXmlAsByte<T>(T dto)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var s = new XmlSerializer(typeof(T));
            using (var stream = new MemoryStream())
            {
                s.Serialize(stream, dto);
                return stream.ToArray();
            }
        }

        /// <summary>
        /// The serialize xml as string.
        /// </summary>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static string SerializeXmlAsString(object dto)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var s = new XmlSerializer(dto.GetType());
            using (var stream = new MemoryStream())
            {
                s.Serialize(stream, dto);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        /// <summary>
        /// The serialize xml as string.
        /// </summary>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static string SerializeXmlAsString<T>(T dto)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var s = new XmlSerializer(typeof(T));
            using (var stream = new MemoryStream())
            {
                s.Serialize(stream, dto);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        /// <summary>
        /// Serialize XML to a file
        /// </summary>
        /// <param name="dto">The object to serialize</param>
        /// <param name="path">The path for the output file</param>
        public static void SerializeXmlToFile(object dto, string path)
        {
            using (StreamWriter writer = new StreamWriter(path, false))
            {
                using (MemoryStream stream = (MemoryStream)SerializationHelper.SerializeXml(dto))
                {
                    stream.WriteTo(writer.BaseStream);
                }
            }
        }

        /// <summary>
        /// Serialize XML to a file
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="dto">The object to serialize</param>
        /// <param name="path">The path for the output file</param>
        public static void SerializeXmlToFile<T>(T dto, string path)
        {
            using (StreamWriter writer = new StreamWriter(path, false))
            {
                using (MemoryStream stream = (MemoryStream)SerializationHelper.SerializeXml<T>(dto))
                {
                    stream.WriteTo(writer.BaseStream);
                }
            }
        }

        /// <summary>
        /// The serialize xml.
        /// </summary>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static Stream SerializeXml<T>(T dto)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var s = new XmlSerializer(typeof(T));
            var stream = new MemoryStream();
            s.Serialize(stream, dto);
            return stream;
        }

        /// <summary>
        /// The serialize xml.
        /// </summary>
        /// <param name="dto">
        /// The dto.
        /// </param>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="Stream"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static Stream SerializeXml<T>(T dto, Stream stream)
        {
            if (dto == null)
            {
                throw new ArgumentNullException("dto");
            }

            var s = new XmlSerializer(typeof(T));
            s.Serialize(stream, dto);
            return stream;
        }

        /// <summary>
        /// The deserialize xml.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static T DeserializeXml<T>(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            stream.Position = 0;
            var s = new XmlSerializer(typeof(T));
            var o = (T)s.Deserialize(stream);
            return o;
        }

        /// <summary>
        /// The deserialize xml.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static T DeserializeXml<T>(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var s = new XmlSerializer(typeof(T));
            using (var stream = new MemoryStream(data))
            {
                var o = (T)s.Deserialize(stream);
                return o;
            }
        }

        /// <summary>
        /// The deserialize xml.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <typeparam name="T">Type of object
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">Exception if object is null
        /// </exception>
        public static T DeserializeXml<T>(string data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var s = new XmlSerializer(typeof(T));

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
            {
                var o = (T)s.Deserialize(stream);
                return o;
            }
        }
        #endregion
    }
}