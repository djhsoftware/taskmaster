﻿
namespace Assets.Shared.Scripts.Utilities
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;

    [System.Serializable]
    public class AppSettings
    {
        public string Server { get; set; }
        public int Port { get; set; }

        public static AppSettings Load()
        {
            AppSettings settings;
            if (File.Exists(Application.persistentDataPath + "/settings.gd"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/settings.gd", FileMode.Open);
                settings = (AppSettings)bf.Deserialize(file);
                file.Close();
            }
            else
            {
                settings = new AppSettings()
                {
                    Server = "tm.djhsoftware.com",
                    Port = 6550
                };
            }

            return settings;
        }

        public void Save()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/settings.gd");
            bf.Serialize(file, this);
            file.Close();
        }
    }
}
