﻿
namespace TaskMaster.Common
{
    using Assets.Shared.Scripts.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;
    using Debug = UnityEngine.Debug;

    public class TMNetworkClient : MonoBehaviour
    {
        public GameObject ProgressBar;
        public TMMediaItem SelectedMedia = null;
        public TMClientType clientIdentity;

        protected bool offline = true;
        protected NetworkClient client;
        protected TMConfiguration Configuration = null;
        protected List<byte[]> dataChunks;
        protected float configProgressDelta;
        protected Image progressBarImage;
        protected AppSettings Settings;

        // Start is called before the first frame update
        protected void NetStart()
        {
            // Load Application settings
            Settings = AppSettings.Load();

            DontDestroyOnLoad(this);
            NetworkServer.Reset();
            client = new NetworkClient();
            client.RegisterHandler(MsgType.Disconnect, OnDisconnect);
            client.RegisterHandler(TaskMasterMsg.IdentifyRequest, OnReceiveIdentifyRequest);
            client.RegisterHandler(TaskMasterMsg.ConfigData, OnReceiveConfiguration);
            client.RegisterHandler(TaskMasterMsg.UpdateScore, OnReceiveScoreUpdate);
            client.RegisterHandler(TaskMasterMsg.SetSelectedMedia, OnSetSelectedMedia);
            client.RegisterHandler(TaskMasterMsg.DataChunk, OnReceiveDataChunk);
            client.RegisterHandler(TaskMasterMsg.ConfigComplete, OnReceiveConfigComplete);
            client.RegisterHandler(TaskMasterMsg.TaskStatusUpdate, OnTaskStatusUpdate);
            client.RegisterHandler(TaskMasterMsg.ShowScene, OnShowScene);
        }

        #region Initialise events
        /// <summary>
        /// Connect to server
        /// </summary>
        public void Connect()
        {
            if (client.isConnected)
            {
                return;
            }

            if (ProgressBar != null)
            {
                ProgressBar.SetActive(true);
            }

            NetworkServer.Reset();

            SetMessage(string.Format("TX:Connect [Host:{0}:{1}]", Settings.Server, Settings.Port));
            client.Connect(Settings.Server, Settings.Port);
        }

        /// <summary>
        /// Disconnect from server
        /// </summary>
        public void Disconnect()
        {
            SetMessage("TX:Disconnect");
            client.Disconnect();
        }

        /// <summary>
        /// Get configuration
        /// </summary>
        public TMConfiguration GetConfiguration()
        {
            return Configuration;
        }

        /// <summary>
        /// Get BuildInfo string
        /// </summary>
        /// <returns>Build info</returns>
        public string GetBuildInfo()
        {
            string buildDate = string.Empty;
            string version = string.Empty;

            try
            {
                // Get build version
                var assembly = Assembly.GetExecutingAssembly();
                var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

                // Get build date
                var filePath = assembly.Location;
                const int CPeHeaderOffset = 60;
                const int CLinkerTimestampOffset = 8;

                var buffer = new byte[2048];

                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    stream.Read(buffer, 0, 2048);
                }

                var offset = BitConverter.ToInt32(buffer, CPeHeaderOffset);
                var secondsSince1970 = BitConverter.ToInt32(buffer, offset + CLinkerTimestampOffset);
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                var linkTimeUtc = epoch.AddSeconds(secondsSince1970);

                var tz = TimeZoneInfo.Local;
                var localTime = TimeZoneInfo.ConvertTimeFromUtc(linkTimeUtc, tz);

                buildDate = string.Format(" ({0})", localTime.ToShortDateString());
            }
            catch
            {
            }

            return string.Format("TMRemote V{0}{1}", Application.version, buildDate);
        }
        #endregion

        #region Update events
        private void SetLoadProgress(float delta)
        {
            if (progressBarImage != null)
            {
                progressBarImage.fillAmount += delta;
            }
        }

        public virtual void SetMessage(string message)
        {
            Debug.Log(message);
        }
        #endregion

        #region Network Events
        public virtual void OnShowScene(NetworkMessage netMsg)
        {
        }

        /// <summary>
        /// Acknowledge receipt of message
        /// </summary>
        /// <param name="conn">Network Connection to respond on</param>
        /// <param name="bytes">Number of bytes received</param>
        private void SendAck(NetworkConnection conn, int bytes)
        {
            //SetMessage(string.Format("TX:SendAck [Bytes:{0}]", bytes));
            var msg = new AcknowledgeMessage()
            {
                Bytes = bytes
            };

            conn.Send(TaskMasterMsg.SendAck, msg);
        }

        /// <summary>
        /// Receive a request to identify, send back the type of client we are
        /// </summary>
        /// <param name="netMsg">Network message</param>
        private void OnReceiveIdentifyRequest(NetworkMessage netMsg)
        {
            SetMessage(string.Format("RX:OnReceiveIdentifyRequest [CID:{0}]", netMsg.conn.connectionId));
            var msg = new IdentifyResponseMessage()
            {
                AgentName = SystemInfo.deviceName,
                ClientType = clientIdentity
            };

            // Send our response, this will trigger the host to send the configuration
            SetMessage(string.Format("TX:IdentifyResponse [Type:{0}]", msg.ClientType));
            netMsg.conn.Send(TaskMasterMsg.IdentifyResponse, msg);
        }

        /// <summary>
        /// Disconnect received from server
        /// </summary>
        /// <param name="netMsg"></param>
        private void OnDisconnect(NetworkMessage netMsg)
        {
            SetMessage(string.Format("RX:OnDisconnect [CID:{0}]", netMsg.conn.connectionId));
            offline = true;
            if (client.isConnected)
            {
                client.Disconnect();
            }

            Destroy(gameObject);
            SceneManager.LoadScene("Offline");
        }

        /// <summary>
        /// Received when all configuration data has been sent
        /// </summary>
        /// <param name="netMsg">Configuration complete message</param>
        private void OnReceiveConfigComplete(NetworkMessage netMsg)
        {
            SetMessage("RX:OnReceiveConfigComplete");
            offline = false;
            SceneManager.LoadScene("Online");
        }

        /// <summary>
        /// Receive configuration data message
        /// </summary>
        /// <param name="netMsg"></param>
        private void OnReceiveConfiguration(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<ConfigDataMessage>();
            Configuration = SerializationHelper.DeserializeXml<TMConfiguration>(msg.Config);

            // Setup progress message
            SetMessage(string.Format("RX:OnReceiveConfiguration[Players:{0}; Media:{1}; Tasks:{2}", Configuration.Players.Count, Configuration.Media.Count, Configuration.Tasks.Count));
            configProgressDelta = 1.0f / ((float)Configuration.Players.Count + (float)Configuration.Media.Count + (float)Configuration.Tasks.Count);
            SetLoadProgress(configProgressDelta * Configuration.Tasks.Count);

            // Set selected video
            SelectedMedia = Configuration.Media.FirstOrDefault(v => v.Selected);

            // Send ACK
            SendAck(netMsg.conn, 0);
        }

        /// <summary>
        /// Configuration - Data chunk received
        /// </summary>
        /// <param name="netMsg"></param>
        private void OnReceiveDataChunk(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<DataChunkMessage>();
            SetLoadProgress(configProgressDelta);
            if (dataChunks == null)
            {
                dataChunks = new List<byte[]>();
            }

            if (msg.ChunkId != msg.TotalChunks)
            {
                SetMessage(string.Format("RX:OnReceive{0} [Id: {1}; Chunk:{2}/{3}]", msg.ChunkType, msg.Name, msg.ChunkId, msg.TotalChunks));
                dataChunks.Add(msg.Data);
            }
            else
            {
                dataChunks.Add(msg.Data);
                var imageData = dataChunks.SelectMany(byteArr => byteArr).ToArray();
                switch (msg.ChunkType)
                {
                    case TMDataChunkType.Avatar:
                        var player = Configuration.Players.FirstOrDefault(p => p.Name == msg.Name);
                        player.Image = imageData;
                        SetMessage(string.Format("RX:OnReceive{0} [Player: {1}; Bytes:{2}]", msg.ChunkType, player.Name, imageData.Length));
                        break;
                    case TMDataChunkType.MediaThumbnail:
                        var mediaId = int.Parse(msg.Name);
                        var media = Configuration.Media.FirstOrDefault(v => v.Id == mediaId);
                        media.Thumbnail = imageData;
                        SetMessage(string.Format("RX:OnReceive{0} [MediaId:{1}; Bytes:{2}]", msg.ChunkType, mediaId, imageData.Length));
                        break;
                }

                dataChunks = null;
            }

            // Send ACK
            SendAck(netMsg.conn, msg.Data.Length);
        }

        /// <summary>
        /// Receive a score update from host
        /// </summary>
        /// <param name="netMsg"></param>
        private void OnReceiveScoreUpdate(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<UpdateScoreMessage>();
            var player = Configuration.Players.FirstOrDefault(p => p.Name == msg.PlayerName);
            player.Score = msg.Score;
            SetMessage(string.Format("RX:OnReceiveScoreUpdate [Player:{0}; Score:{1}]", player.Name, player.Score));
        }

        /// <summary>
        /// Receive task status update from host
        /// </summary>
        /// <param name="netMsg"></param>
        private void OnTaskStatusUpdate(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<TaskStatusUpdateMessage>();
            SetMessage(string.Format("RX:OnTaskStatusUpdate [CID:{0}; TaskId:{1}; Active:{2}; Complete:{3}]", netMsg.conn.connectionId, msg.TaskId, msg.Active, msg.Complete));
            var task = Configuration.Tasks.FirstOrDefault(t => t.Id == msg.TaskId);
            task.Active = msg.Active;
            task.Complete = msg.Complete;
        }

        /// <summary>
        /// Set the currently selected video
        /// </summary>
        /// <param name="netMsg">Message</param>
        private void OnSetSelectedMedia(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<SetSelectedMediaMessage>();

            // Set selected video
            SetMessage(string.Format("RX:OnSetSelectedMedia [MediaId:{0}]", msg.MediaId));
            foreach (var v in Configuration.Media)
            {
                if (v.Id == msg.MediaId)
                {
                    v.Selected = true;
                    SelectedMedia = v;
                }
                else
                {
                    v.Selected = false;
                }
                v.Selected = v.Id == msg.MediaId;
            }
        }
        #endregion
    }
}
