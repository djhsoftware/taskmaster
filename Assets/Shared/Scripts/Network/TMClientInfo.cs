﻿
namespace TaskMaster.Common
{
    using System.Collections.Generic;
    using UnityEngine.Networking;

    public enum TMClientType
    {
        Unknown,
        Scoreboard,
        Remote
    }

    public enum TMDataChunkType
    {
        MediaThumbnail,
        Avatar
    }

    public enum TMClientStatus
    {
        Unknown,
        Intialising,
        Ready
    }

    public class TMClientInfo
    {
        public TMClientInfo(NetworkConnection conn)
        {
            Connection = conn;
            ClientType = TMClientType.Unknown;
            DataQueue = new Queue<DataChunkMessage>();
        }

        public Queue<DataChunkMessage> DataQueue { get; set; }
        public TMClientType ClientType { get; set; }
        public NetworkConnection Connection { get; set; }
        public TMClientStatus ClientStatus { get; set; }
    }
}
