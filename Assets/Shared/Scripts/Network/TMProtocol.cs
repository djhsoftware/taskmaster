﻿namespace TaskMaster.Common
{
    using UnityEngine.Networking;

    public class TaskMasterMsg
    {
        // Identity events
        public const short IdentifyRequest = 100;
        public const short IdentifyResponse = 101;

        // Configuration events
        public const short SendAck = 200;
        public const short ConfigData = 201;
        public const short DataChunk = 202;
        public const short ConfigMediaThumbnail = 203;
        public const short ConfigComplete = 250;

        // Status update events
        public const short ShowScene = 300;
        public const short UpdateScore = 301;
        public const short SetSelectedMedia = 302;
        public const short TaskStatusUpdate = 303;
    }

    #region Identity events
    /// <summary>
    /// Send to a client to identify what type it is
    /// </summary>
    public class IdentifyRequestMessage : MessageBase
    {
    }

    public class AcknowledgeMessage : MessageBase
    {
        public int Bytes;
    }

    /// <summary>
    /// Send to a client to identify what type it is
    /// </summary>
    public class IdentifyResponseMessage : MessageBase
    {
        public TMClientType ClientType;
        public string AgentName;
    }

    #endregion

    #region Configuration events
    /// <summary>
    /// Configuration Data message
    /// </summary>
    public class ConfigDataMessage : MessageBase
    {
        public string Config;
    }

    public class ConfigCompleteMessage : MessageBase
    {
        public string Reserved;
    }

    /// <summary>
    /// General Data Chunk Message
    /// </summary>
    public class DataChunkMessage : MessageBase
    {
        public TMDataChunkType ChunkType;
        public int ChunkId;
        public int TotalChunks;
        public string Name;
        public byte[] Data;
    }
    #endregion

    #region Status update events
    public class ShowSceneMessage : MessageBase
    {
        public string SceneName;
    }

    public class SetSelectedMediaMessage : MessageBase
    {
        public int MediaId;
    }

    public class SetScoreboardMessage : MessageBase
    {
        public int[] Scores;
    }

    public class UpdateScoreMessage : MessageBase
    {
        public string PlayerName;
        public int Score;
    }

    public class TaskStatusUpdateMessage : MessageBase
    {
        public int TaskId;
        public bool Complete;
        public bool Active;
    }
    #endregion
}